# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy/compare/0.1.0...0.2.0) (2020-05-13)


### ⚠ BREAKING CHANGES

* The `region` variable was unused and was removed, if you were passing it then you should stop passing it.

### Features

* Support 'stage' and 'environment' label params ([0eeccc2](https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy/commit/0eeccc22d4e95fc0033d2f32fc861ec8a216cefa))


### Bug Fixes

* remove unused region variable ([d442d21](https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy/commit/d442d216793575c8204486c8ef9f005698b0b492))

## [0.1.0][0.1.0] - 2020-04-04

### Added

- This CHANGELOG
- Initial version

### Changed

n/a

### Removed

n/a

[0.1.0]: https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy/tags/0.1.0
